// ==UserScript==
// @name        orpheus-populator-dev
// @version     0.0.0
// @author      cheesegoat
// @description Interface to move own RED uploads to OPS
// @match       https://orpheus.network/*
// @namespace   cheesegoat.user.orpheus.network
// @grant       GM.xmlHttpRequest
// ==/UserScript==


(function() {
  'use strict';

  let el = document.createElement('app-root');
  document.body.appendChild(el);
  // FIXME: :(
  unsafeWindow.GM = GM;
  let files = [
    'runtime', 'polyfills', 'styles', 'vendor', 'main'
  ];
  let ts = +new Date();
  for (let file of files) {
    let el = document.createElement('script');
    el.src = 'http://localhost:4200/' + file + '.js?ts='+ts;
    document.body.appendChild(el);
  }
})();
