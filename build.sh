#!/bin/sh

BUILD_PATH="dist/orpheus-populator"
OUTFILE="orpheus-populator.user.js"

npm run ng build -- --prod

cp "${BUILD_PATH}"/runtime.*.js "${BUILD_PATH}/${OUTFILE}"
grep -v '^//' "${BUILD_PATH}"/polyfills.*.js >> "${BUILD_PATH}/${OUTFILE}"
cat <<EOF >> "${BUILD_PATH}/${OUTFILE}"
(function() {
    'use strict';
    const el = document.createElement('app-root');
    document.body.appendChild(el);
})();
EOF

grep -v '^//' "${BUILD_PATH}"/main.*.js >> "${BUILD_PATH}/${OUTFILE}"
