# orpheus-populator

This is a userscript that allows to move uploads of a specific RED user account to OPS through an adaptive user interface on OPS.  

## Install

Compatibility was only tested with Chrome v83 and newer.

Releases can be found at https://gitlab.com/cheesegoat/orpheus-populator/-/releases

Add the provided `orpheus-populator.user.js` to your Greasemonkey/Tampermonkey.

## Basic usage

While on the OPS site, you will see a small double-arrow-icon in the center of
of each page border. They can be used to open/close the corresponding panel.

Functionality for each:
left: torrent list
right: settings
top/bottom: torrent detail view

All open panels can at any time be closed by hitting the Escape button on
your keyboard or clicking the corresponding arrow icon again.

## Development

This userscript is written using the Angular2 framework.

```
git clone https://gitlab.com/cheesegoat/orpheus-populator
cd orpheus-populator
npm install
```

After those commands Angular2 and all dependencies for this project should be installed.

Install the `greasemonkey-dev.js` userscript using Greasemonkey/Tampermonkey and start the Angular2 development server using `ng serve` (the ng executable can be found at `node_modules/@angular/cli/bin/ng`). This will start a http server at `localhost:4200` which will be accessed by the userscript.

## Build

Run the `build.sh` script for creating a single distributable userscript file. The result will be saved at `dist/orpheus-populator/orpheus-populator.user.js`.
