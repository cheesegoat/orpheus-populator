const WebpackUserscript = require('webpack-userscript')

module.exports = {
  plugins: [
    new WebpackUserscript({
      headers: 'src/greasemonkey-header.json',
      metajs: false,
      renameExt: false,
      pretty: true,
      proxyScript: {enable: false}
    })
  ]
}
