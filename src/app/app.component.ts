import {Component, ComponentFactoryResolver} from '@angular/core';
import { ViewContainerRef } from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {animate, style, transition, trigger} from '@angular/animations';
import { AllHtmlEntities } from 'html-entities';
import {decode, encode} from 'bencode';
import {DatalistComponent} from './orpheus-populator/datalist/datalist.component';
import {SettingsComponent} from './orpheus-populator/settings/settings.component';
import {TorrentdetailsComponent} from './orpheus-populator/torrentdetails/torrentdetails.component';
import {EntrySettingsService} from './orpheus-populator/torrententry/entrysettings.service';
import {TorrentDetailsService} from './orpheus-populator/torrentdetails/torrentdetails.service';
import {TorrentDataService, TorrentEntry} from './orpheus-populator/datalist/torrent-data.service';
import {SettingsService} from './orpheus-populator/settings/settings.service';


const encoder = new AllHtmlEntities();


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('showToggleLeft', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('100ms')
      ]),
      transition(':leave', [
        animate('100ms', style({ transform: 'translateX(-100%)' }))
      ]),
    ]),
    trigger('showToggleRight', [
      transition(':enter', [
        style({ transform: 'translateX(200%)' }),
        animate('200ms')
      ]),
      transition(':leave', [
        animate('200ms', style({ transform: 'translateX(200%)' }))
      ]),
    ]),
    trigger('showToggleTop', [
      transition(':enter', [
        style({ transform: 'translateY(-100%)' }),
        animate('100ms')
      ]),
      transition(':leave', [
        animate('100ms', style({ transform: 'translateY(-100%)' }))
      ]),
    ]),
    trigger('showToggleBottom', [
      transition(':enter', [
        style({ transform: 'translateY(200%)' }),
        animate('200ms')
      ]),
      transition(':leave', [
        animate('200ms', style({ transform: 'translateY(200%)' }))
      ]),
    ])
  ]
})
export class AppComponent {
  title = 'red-move';
  isShown: Map<string, boolean> = new Map();

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private entrySettingsSvc: EntrySettingsService,
              private torrentDetailsService: TorrentDetailsService,
              private torrentDataService: TorrentDataService,
              private settingsService: SettingsService) {
    entrySettingsSvc.addAction({
      text: 'top',
      action: (entry) => this.torrentDetailsService.setEntry(entry, 'top')
    });
    entrySettingsSvc.addAction({
      text: 'bottom',
      action: (entry) => this.torrentDetailsService.setEntry(entry, 'bottom')
    });
    if (document.querySelector('#torrent-json-file') !== null) {
      entrySettingsSvc.addAction({
        text: 'load',
        action: (entry) => this.insertTorrent(entry)
      });
    }
    let w = window;
    // @ts-ignore
    if (typeof unsafeWindow !== 'undefined') {
      // A userscript's window is sandboxed. For global shortcuts we need
      // the real window.
      // @ts-ignore
      w = unsafeWindow;
    }
    w.addEventListener('keyup', e => {
      if (e.key === 'Escape') {
        for (const label of this.isShown) {
          this.isShown.set(label[0], false);
        }
      }
    });
  }

  getComponent(id: string): any {
    if (id === 'left') {
      return DatalistComponent;
    } else if (id === 'right') {
      return SettingsComponent;
    } else {
      return TorrentdetailsComponent;
    }
  }

  insertTorrent(entry: TorrentEntry): void {
    // @ts-ignore
    insertParsedJson(this.transformTorrentEntry(entry));
    this.torrentDataService.getRemoteTorrentFile(entry.data.id).subscribe(
      torrentData => {
        // create new torrent file
        // @ts-ignore
        const announceUrl: string = document.querySelector('input[value$="/announce"]').value;
        const torrentDict = decode(torrentData);
        torrentDict.announce = announceUrl;
        torrentDict.info.source = 'OPS';
        const newTorrent = encode(torrentDict);
        const fileBlob = new Blob([newTorrent],
          {type: 'application/x-bittorrent'});

        // replace torrent file input field with download link
        const formInputFileTd = document.querySelector('#file').parentElement;
        const downloadLink = document.createElement('a');
        downloadLink.textContent = 'download new torrent';
        downloadLink.target = '_blank';
        downloadLink.download = entry.data.filePath + '.torrent';
        downloadLink.href = URL.createObjectURL(fileBlob);

        // make OPS form checker js happy
        const fileDummy = document.createElement('input');
        fileDummy.type = 'hidden';
        fileDummy.id = 'file';
        fileDummy.value = 'orpheus-populator dummy string';

        formInputFileTd.innerHTML = '';
        formInputFileTd.appendChild(fileDummy);
        formInputFileTd.appendChild(downloadLink);

        // set up handler for upload form submit
        const domForm = document.querySelector('#upload_table');
        domForm.addEventListener('submit', (e) => {
          e.preventDefault();
          // @ts-ignore
          // tslint:disable-next-line:no-unused-expression
          const uploadForm = new FormData(e.target);

          // check whether we should have and do have a log file
          if (uploadForm.get('media') === 'CD'
              && uploadForm.get('format') === 'FLAC'
              && entry.data.hasLog) {
            let hasLogfile = false;
            for (const lf of uploadForm.getAll('logfiles[]')) {
              // @ts-ignore
              if (lf.size > 0) {
                hasLogfile = true;
                break;
              }
            }
            if (!hasLogfile) {
              const answer = confirm('Warning: no log files specified! Continue?');
              if (!answer) {
                document.querySelector('#post')
                  .removeAttribute('disabled');
                return;
              }
            }
          }
          uploadForm.set('file_input', fileBlob, 'blob.torrent');
          const request = new XMLHttpRequest();
          request.onloadend = (x) => {
            // @ts-ignore
            if (x.target.responseURL === e.target.action) {
              // an error occurred, extract error message and show user
              let errorMsg;
              try {
                // @ts-ignore
                errorMsg = x.target.responseXML
                  .querySelector('#upload_table')
                  .previousElementSibling.textContent;
              } catch (e) {
                console.log('orpheus upload error response: ', x);
                errorMsg = 'unknown error';
              }
              alert('Error while uploading: ' + errorMsg);
              document.querySelector('#post')
                .removeAttribute('disabled');
            } else {
              if (this.settingsService.get('redirectOnSuccess')) {
                // @ts-ignore
                window.location.href = x.target.responseURL;
              } else {
                alert('Torrent successfully uploaded!');
              }
            }
          };
          // @ts-ignore
          request.open(e.target.method, e.target.action);
          request.responseType = 'document';
          request.send(uploadForm);
        });
      }
    );
  }

  /**
   * Transform a TorrentEntry's JSON data into the format
   * expetced by the OPS JSON loader.
   */
  transformTorrentEntry(entry: TorrentEntry): any {
    const json: any = entry.data.jsonObject;
    if (json.group.bbBody) {
      json.group.wikiBody = encoder.decode(json.group.bbBody);
      json.group.bbBody = encoder.decode(json.group.bbBody);
    }
    json.group.wikiImage = entry.group.wikiImage;
    json.group.name = entry.group.name;
    json.group.catalogueNumber = entry.group.catalogueNumber;
    json.torrent.remasterTitle = entry.data.remasterTitle;
    json.torrent.remasterRecordLabel = entry.data.remasterRecordLabel;
    json.torrent.remasterCatalogueNumber = entry.data.remasterCatalogueNumber;
    json.torrent.description = entry.data.description;
    return {response: json};
  }
}
