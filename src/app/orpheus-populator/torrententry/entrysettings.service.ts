import {Injectable} from '@angular/core';
import {TorrentEntry} from '../datalist/torrent-data.service';


export interface LinkAction {
  text: string;
  action: (entry: TorrentEntry) => void;
}

/**
 * Provides a way to register action links for all TorrentEntry components.
 */
@Injectable({
  providedIn: 'root'
})
export class EntrySettingsService {
  public actions: LinkAction[] = [];
  public addAction(la: LinkAction): void {
    this.actions.push(la);
  }
  public removeAction(text: string): void {
    let foundAction = null;
    let index = 0;
    for (const action of this.actions) {
      if (action.text === text) {
        foundAction = action;
        break;
      }
      index++;
    }
    if (foundAction !== null) {
      this.actions.splice(index, 1);
    }
  }
}
