import {Component, Input, OnInit} from '@angular/core';
import {TorrentEntry} from '../datalist/torrent-data.service';
import {EntrySettingsService} from './entrysettings.service';


/**
 * Provide a concise view of a specific torrent along with globally registered
 * action links. Used in the Datalist component.
 */
@Component({
  selector: 'app-torrententry',
  templateUrl: './torrententry.component.html',
  styleUrls: ['./torrententry.component.css']
})
export class TorrententryComponent implements OnInit {
  @Input() entry: TorrentEntry;
  edition: [string, number, string, string];
  constructor(public entrySettingsSvc: EntrySettingsService) {}
  ngOnInit(): void {
    this.edition = this.entry.getEditionInfo();
  }
}
