import {Component, Input, OnInit} from '@angular/core';
import {SettingsService} from './settings.service';
import {FormControl} from '@angular/forms';
import {LayoutChildComponent} from '../layout/layout.component';
import {torrentListTypesArr} from '../datalist/torrent-data.service';


/**
 * Basic settings manager used by other components.
 */
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, LayoutChildComponent {
  torrentListTypesArr = torrentListTypesArr;

  @Input() position: string;
  remoteBaseUrl = new FormControl('');
  remoteApiKey = new FormControl('');
  userId = new FormControl('');
  torrentListType = new FormControl('');
  limit = new FormControl('');
  offset = new FormControl('');
  redirectOnSuccess = new FormControl('');

  constructor(private settings: SettingsService) { }

  ngOnInit(): void {
    for (const key of Object.keys(SettingsService.defaultSettings)) {
      this[key].setValue(this.settings.get(key));
      this[key].valueChanges.forEach(x => this.updateValue(key, x));
    }
  }

  private updateValue(key: string, value: any): void {
    this.settings.set(key, value);
  }
}
