import {Injectable, Inject} from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import {TorrentListTypes} from '../datalist/torrent-data.service';


interface Settings {
  remoteBaseUrl: string;
  remoteApiKey: string;
  userId: number;
  limit: number;
  offset: number;
  redirectOnSuccess: boolean;
  torrentListType: TorrentListTypes;
}

/**
 * Provides access to application-wide settings.
 */
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  public static defaultSettings: Settings = {
    remoteBaseUrl: 'https://redacted.ch/',
    remoteApiKey: '',
    userId: 0,
    limit: 10,
    offset: 0,
    redirectOnSuccess: true,
    torrentListType: 'uploaded',
  };
  private static keyPrefix = 'red-move-';

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {}

  set(key: string, value: any): void {
    if (!SettingsService.defaultSettings[key] === undefined) {
      throw new Error(`unknown setting: '${key}'`);
    }
    this.storage.set(SettingsService.keyPrefix + key, value);
  }

  get(key: string): any {
    if (!SettingsService.defaultSettings[key] === undefined) {
      throw new Error(`unknown setting: '${key}'`);
    }
    const value = this.storage.get(SettingsService.keyPrefix + key);
    return value !== undefined ? value : SettingsService.defaultSettings[key];
  }
}
