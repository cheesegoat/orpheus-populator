import {Component, ComponentFactoryResolver, Directive, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

export interface LayoutChildComponent {
  position: string;
}


@Directive({selector: 'layout-content'})
export class ContentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

/**
 * Wrapper to allow placing individual components dynamically in each
 * layout component.
 */
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  @Input() component: any;
  @Input() position: string;
  @ViewChild(ContentDirective, {static: true}) contentDirective: ContentDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component);

    const viewContainerRef = this.contentDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    (componentRef.instance as LayoutChildComponent).position = this.position;
  }

}
