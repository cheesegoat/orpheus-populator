import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatalistComponent } from './datalist/datalist.component';
import { HttpClientModule } from '@angular/common/http';
import { TorrententryComponent } from './torrententry/torrententry.component';
import { SettingsComponent } from './settings/settings.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ContentDirective, LayoutComponent} from './layout/layout.component';
import {TorrentdetailsComponent, TorrentFileTreeComponent} from './torrentdetails/torrentdetails.component';



@NgModule({
  declarations: [DatalistComponent, TorrententryComponent, SettingsComponent, LayoutComponent, ContentDirective, TorrentdetailsComponent, TorrentFileTreeComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
  exports: [
    DatalistComponent,
    LayoutComponent
  ]
})
export class OrpheusPopulatorModule { }
