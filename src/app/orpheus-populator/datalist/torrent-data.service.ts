import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AllHtmlEntities } from 'html-entities';
import { SettingsService } from '../settings/settings.service';

/*
 * Handles API requests to Gazelle and boxes responses in corresponding
 * objects.
 */


const encoder = new AllHtmlEntities();


class ApiResponse {
  constructor(public status: string, public response: any) {}

  public isValid(): boolean {
    return this.status === 'success';
  }
}


export class SortedTorrentData {
  categories = ['seeding', 'leeching', 'uploaded', 'snatched'];
  seeding: Map<number, TorrentDataStub> = new Map();
  leeching: Map<number, TorrentDataStub> = new Map();
  uploaded: Map<number, TorrentDataStub> = new Map();
  snatched: Map<number, TorrentDataStub> = new Map();

  constructor(private source: string, private data: object, private torrentDataService?: TorrentDataService) {
    for (const category of this.categories) {
      if (data[category]) {
        this[category] = new Map(data[category].map(x =>
          [x.torrentId, new TorrentDataStub(
            torrentDataService,
            source,
            new Artist(x.artistId, x.artistName),
            x.groupId, x.name, x.torrentId)]));
      }
    }
  }

  public merge(other: SortedTorrentData): void {
    for (const category of this.categories) {
      for (const td of other[category]) {
        this[category].set(td[0], td[1]);
      }
    }
  }
}


export class Artist {
  constructor(public artistId: number, public name: string) {
    this.name = encoder.decode(this.name);
  }
}


export class ArtistInfo {
  static artistTypes = ['composers', 'dj', 'artists', 'guests',
                        'conductor', 'remixedBy', 'producer'];
  constructor(
    public composers: Artist[],
    public dj: Artist[],
    public artists: Artist[],
    public guests: Artist[],
    public conductor: Artist[],
    public remixedBy: Artist[],
    public producer: Artist[],
  ) {
    this.ensureSorted();
  }

  public static fromObject(obj: any): ArtistInfo {
    const info = new ArtistInfo([], [], [], [], [], [], []);
    for (const type of ArtistInfo.artistTypes) {
      const jsonType = type === 'guests' ? 'with' : type;
      for (const ad of obj[jsonType]) {
        info[type].push(new Artist(ad.id, ad.name));
      }
    }
    info.ensureSorted();
    return info;
  }

  ensureSorted(): void {
    for (const type of ArtistInfo.artistTypes) {
      this[type].sort();
    }
  }
}

export class PathEntry {
  constructor(public name: string, public size: number,
              public isFolder: boolean,
              public children: Map<string, PathEntry>) {}

  getChild(name: string): PathEntry {
    if (this.children) {
      return this.children.get(name);
    }
    return undefined;
  }

  addChild(path: PathEntry): void {
    if (!this.children) {
      this.children = new Map();
    }
    this.children.set(path.name, path);
  }

  getTotalSize(): number {
    if (!this.isFolder) {
      return this.size;
    }
    let size = 0;
    for (const child of this.children) {
      size += child[1].getTotalSize();
    }
    return size;
  }

  getHumanReadableSize(): string {
    const size = this.getTotalSize();
    const i = size === 0 ? 0 : Math.floor( Math.log(size) / Math.log(1024) );
    const a = Number(( size / Math.pow(1024, i) ).toFixed(2));
    return a + ' ' + ['B', 'kiB', 'MiB', 'GiB', 'TiB'][i];
  }
}

export class TorrentGroup {
  constructor(
    public wikiBody: string,
    public wikiImage: string,
    public id: number,
    public name: string,
    public year: number,
    public recordLabel: string,
    public catalogueNumber: string,
    public releaseType: number,
    public categoryId: number,
    public categoryName: string,
    public time: string,
    public vanityHouse: boolean,
    public artistInfo: ArtistInfo,
    public torrents: TorrentData[]
  ) {
    this.wikiBody = encoder.decode(this.wikiBody);
    this.wikiImage = encoder.decode(this.wikiImage);
    this.name = encoder.decode(this.name);
    this.recordLabel = encoder.decode(this.recordLabel);
    this.catalogueNumber = encoder.decode(this.catalogueNumber);
  }
}


export class TorrentData {
  public fileTree: PathEntry;
  public audioFileCount: number;
  constructor(
    public id: number,
    public media: string,
    public format: string,
    public encoding: string,
    public remastered: boolean,
    public remasterYear: number,
    public remasterTitle: string,
    public remasterRecordLabel: string,
    public remasterCatalogueNumber: string,
    public scene: boolean,
    public hasLog: boolean,
    public hasCue: boolean,
    public logScore: number,
    public fileCount: number,
    public seeders: number,
    public leechers: number,
    public snatched: number,
    public freeTorrent: boolean,
    public time: string,
    public description: string,
    public fileList: string,
    public filePath: string,
    public userId: number,
    public username: string,
    public jsonObject: object
  ){
    this.remasterTitle = encoder.decode(this.remasterTitle);
    this.remasterRecordLabel = encoder.decode(this.remasterRecordLabel);
    this.remasterCatalogueNumber = encoder.decode(this.remasterCatalogueNumber);
    this.description = encoder.decode(this.description);
    this.fileList = encoder.decode(this.fileList);
    this.filePath = encoder.decode(this.filePath);
    this.fileTree = this.parseFileList();
    this.audioFileCount = this.countAudioFiles(this.fileTree);
  }

  private parseFileList(): PathEntry {
    const root = new PathEntry(null, 0, true, null);
    const files = this.fileList.split('|||');
    for (const file of files) {
      const fileInfo = file.split('{{{');
      const filePath = fileInfo[0];
      const fileSize = Number(fileInfo[1].replace('}}}', ''));
      let parent = root;
      for (const path of filePath.split('/')) {
        let thisChild = parent.getChild(path);
        if (thisChild === undefined) {
          thisChild = new PathEntry(path, 0, true, null);
          parent.addChild(thisChild);
        }
        parent = thisChild;
      }
      if (parent === root) {
        throw new Error('Received invalid file list: no files!');
      }
      parent.isFolder = false;
      parent.size = fileSize;
    }
    return root;
  }

  private countAudioFiles(path: PathEntry): number {
    if (path.isFolder) {
      let count = 0;
      for (const child of path.children) {
        count += this.countAudioFiles(child[1]);
      }
      return count;
    }
    const ext = path.name.split('.').pop().toLowerCase();
    if (ext === 'mp3' || ext === 'flac' || ext === 'm4a') {
      return 1;
    }
    return 0;
  }
}


export class TorrentEntry {
  constructor(public group: TorrentGroup, public data: TorrentData) {}

  public getDisplayArtist(): string {
    const artists = this.group.artistInfo;
    let artistStr;
    // TODO: lazy and simplified
    if (artists.artists.length === 1) {
      artistStr = artists.artists[0].name;
    } else if (artists.artists.length === 2) {
      artistStr = `${artists.artists[0].name} + ${artists.artists[1].name}`;
    } else if (artists.artists.length === 0) {
      artistStr = 'Unknown Artist';
    } else  {
      artistStr = 'Various Artists';
    }
    return artistStr;
  }

  public getEditionInfo(): [string, number, string, string] {
    if (this.data.remastered) {
      return [
        this.data.remasterTitle || '',
        this.data.remasterYear || 0,
        this.data.remasterRecordLabel || '',
        this.data.remasterCatalogueNumber || ''
      ];
    } else {
      return [
        'Original Release',
        this.group.year || 0,
        this.group.recordLabel || '',
        this.group.catalogueNumber || ''
      ];
    }
  }

  public getSearchOptions(): { [param: string]: string ; } {
    const options: { [param: string]: string ; } = {
      media: this.data.media,
      format: this.data.format,
      encoding: this.data.encoding,
      groupname: this.group.name,
      year: this.group.year.toString()
    };
    if (this.group.artistInfo.artists.length >= 1) {
      options.artistname = this.group.artistInfo.artists[0].name;
    }
    return options;
  }
}


export class TorrentDataStub {
  details?: TorrentEntry;
  constructor(private torrentDataService: TorrentDataService,
              public source: string,
              public artist: Artist,
              public groupId: number, public name: string,
              public torrentId: number) {
    this.name = encoder.decode(this.name);
    this.retrieveTorrentDetails();
  }
  retrieveTorrentDetails(): void {
    this.torrentDataService.getTorrent(this.torrentId, this.source)
      .subscribe(x => this.details = x);
  }
}

export const torrentListTypesArr = ['uploaded', 'snatched', 'seeding', 'leeching'] as const;
export type TorrentListTypes = typeof torrentListTypesArr[number];

@Injectable({
  providedIn: 'root'
})
export class TorrentDataService {
  constructor(private client: HttpClient,
              private settings: SettingsService) { }

  public getDataList(type: TorrentListTypes, userId: number): Observable<SortedTorrentData> {
    // TODO: pagination
    const apiData: Observable<SortedTorrentData> = this.makeRemoteApiRequest(
      `ajax.php?action=user_torrents&type=${type}&id=${this.settings.get('userId')}&limit=${this.settings.get('limit')}&offset=${this.settings.get('offset')}`)
    .pipe(
      map(res => {
        res = new ApiResponse(res.status, res.response);
        if (!res.isValid()) {
          throw new Error('Unable to retrive data from RED: ' + res);
        }
        return new SortedTorrentData('remote', res.response, this);
      })
    );
    return apiData;
  }

  public getTorrent(torrentId: number, source: string): Observable<TorrentEntry> {
    let api = 'makeRemoteApiRequest';
    if (source === 'local') {
      api = 'makeLocalApiRequest';
    }
    const apiData: Observable<TorrentEntry> = this[api](
      `ajax.php?action=torrent&id=${torrentId}`)
      .pipe(
        map(res => {
          // @ts-ignore
          const apires = new ApiResponse(res.status, res.response);
          if (!apires.isValid()) {
            throw new Error('Unable to retrive data from RED: ' + res);
          }
          const td = apires.response.torrent;
          const gd = apires.response.group;
          const artists = ArtistInfo.fromObject(gd.musicInfo);
          const tdata = new TorrentData(
            td.id,
            td.media,
            td.format,
            td.encoding,
            td.remastered,
            td.remasterYear,
            td.remasterTitle,
            td.remasterRecordLabel,
            td.remasterCatalogueNumber,
            td.scene,
            td.hasLog,
            td.hasCue,
            td.logScore,
            td.fileCount,
            td.seeders,
            td.leechers,
            td.snatched,
            td.freeTorrent,
            td.time,
            td.description,
            td.fileList,
            td.filePath,
            td.userId,
            td.username,
            apires.response
          );
          const group = new TorrentGroup(
            gd.wikiBody,
            gd.wikiImage,
            gd.id,
            gd.name,
            gd.year,
            gd.recordLabel,
            gd.catalogueNumber,
            gd.releaseType,
            gd.categoryId,
            gd.categoryName,
            gd.time,
            gd.vanityHouse,
            artists,
            [tdata]);
          return new TorrentEntry(group, tdata);
        })
      );
    return apiData;
  }

  public searchLocalTorrent(searchOptions: { [param: string]: string | string[]; }): Observable<Observable<TorrentEntry>[]> {
    const apiData: Observable<Observable<TorrentEntry>[]> = this.makeLocalApiRequest(
      'ajax.php?action=browse', searchOptions)
      .pipe(
        map(res => {
          res = new ApiResponse(res.status, res.response);
          if (!res.isValid()) {
            throw new Error('Unable to retrive data from RED: ' + res);
          }
          const out = [];
          const results = res.response.results;
          for (const group of results) {
            for (const torrent of group.torrents) {
              out.push(this.getTorrent(torrent.torrentId, 'local'));
            }
          }
          return out;
        }));
    return apiData;
  }

  public getRemoteTorrentFile(torrentId: number): Observable<any> {
    return new Observable<ApiResponse>(observer => {
      // @ts-ignore
      GM.xmlHttpRequest({
        url: this.settings.get('remoteBaseUrl') + 'ajax.php?action=download&id=' + Number(torrentId),
        'headers': this.getRemoteAuthHeaders(),
        method: 'GET',
        responseType: 'arraybuffer',
        onload: res => observer.next(res.response)
      });
    });
  }

  private getRemoteAuthHeaders(): any {
    let headers;
    if (this.settings.get('remoteApiKey')) {
      headers = {Authorization: this.settings.get('remoteApiKey')};
    } else {
      headers = {};
    }
    return headers;
  }

  private makeApiRequest(urlbase: string, uri: string,
                         params?: { [param: string]: string | string[]; },
                         headers?: { [param: string]: string | string[]; }): Observable<ApiResponse> {
    /*return this.client.get<ApiResponse>(
      urlbase + uri,
      { 'headers': headers, 'params': params });*/
    // RED does not correctly respond to OPTIONS requests, therefore
    // we need to use the privileged GreaseMonkey API
    // Don't do this at home, kids!
    return new Observable<ApiResponse>(observer => {
      let paramStr = '';
      if (params) {
        // @ts-ignore
        paramStr = '&' + Object.entries(params).map(kv => kv.map(encodeURIComponent).join('=')).join('&');
      }
      // @ts-ignore
      GM.xmlHttpRequest({
        url: urlbase + uri + paramStr,
        'headers': headers,
        method: 'GET',
        onload: res => observer.next(JSON.parse(res.response)),
        onerror: res => {
          alert('Error retrieving data from API.');
          console.log('api error', res);
        }
      });
    });
  }

  private makeLocalApiRequest(uri: string, params?: { [param: string]: string | string[]; }): Observable<ApiResponse> {
    return this.makeApiRequest('/', uri, params);
  }

  private makeRemoteApiRequest(
      uri: string, params?: { [param: string]: string | string[]; }): Observable<ApiResponse> {
    return this.makeApiRequest(
      this.settings.get('remoteBaseUrl'), uri, params, this.getRemoteAuthHeaders());
  }
}
