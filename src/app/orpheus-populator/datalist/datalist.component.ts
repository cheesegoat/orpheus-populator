import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TorrentDataService, SortedTorrentData, TorrentDataStub, TorrentEntry} from './torrent-data.service';
import {style, animate, transition, trigger} from '@angular/animations';
import {LayoutChildComponent} from '../layout/layout.component';
import {EntrySettingsService} from '../torrententry/entrysettings.service';
import {SettingsService} from '../settings/settings.service';

/**
 * Responsible for displaying groups of torrents in a quick-view fashion
 * with action links. (eg uploaded, search results)
 */
@Component({
  selector: 'app-datalist',
  templateUrl: './datalist.component.html',
  styleUrls: ['./datalist.component.css'],
  animations: [
    trigger('shrinkOut', [
      transition(':enter', [
        style({ height: 0 }),
        animate('100ms', style({ height: '*' }))
      ]),
      transition(':leave', [
        animate('100ms', style({ height: 0 }))
      ]),
    ])
  ],
})
export class DatalistComponent implements OnInit, OnDestroy, LayoutChildComponent {
  @Input() position: string;
  isCollapsed: Map<string, boolean> = new Map();

  sortedTorrentData = new SortedTorrentData('remote', {});
  searchResults: TorrentEntry[] = [];

  constructor(private torrentDataService: TorrentDataService,
              private entrySettingsSvc: EntrySettingsService,
              private settingsService: SettingsService,
              private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.entrySettingsSvc.addAction({
      text: 'search',
      action: (entry) => this.searchTorrent(entry)
    });
    this.getTorrentData();
  }

  ngOnDestroy(): void {
    this.entrySettingsSvc.removeAction('search');
  }

  getTorrentData(): void {
    this.torrentDataService.getDataList(
        this.settingsService.get('torrentListType'),
        this.settingsService.get('userId'))
      .subscribe(x => {
        this.sortedTorrentData.merge(x);
        // FIXME: only needed when using GM.xmlHttpRequest
        // for reasons unknown
        const intervalId = setInterval(e => {
          let done = true;
          for (const cat of this.sortedTorrentData.categories) {
            for (const td of this.sortedTorrentData[cat].values<TorrentDataStub>()) {
              if (!td.details) {
                done = false;
                break;
              }
            }
          }
          if (done) {
            clearInterval(intervalId);
          }
          this.cdr.detectChanges();
        }, 200);
      }
    );
  }

  getTorrentDataValues(category: string): TorrentDataStub[] {
    return [...this.sortedTorrentData[category].values()];
  }

  getCollapsed(category): boolean {
    return this.isCollapsed.get(category) || false;
  }

  toggleCollapsed(category): void {
    this.isCollapsed.set(category, !this.getCollapsed(category));
    // FIXME: only needed when using GM.xmlHttpRequest
    // for reasons unknown
    this.cdr.detectChanges();
  }

  searchTorrent(source: TorrentEntry): void {
    this.searchResults = [];
    this.cdr.detectChanges();
    this.torrentDataService.searchLocalTorrent(source.getSearchOptions())
      .subscribe(entries => {
        if (entries.length < 1) {
          alert('No search results');
          return;
        }
        for (const entry of entries) {
          entry.subscribe(y => {
            this.searchResults.push(y);
            // FIXME: only needed when using GM.xmlHttpRequest
            // for reasons unknown
            this.cdr.detectChanges();
          });
        }
      });
  }
}
