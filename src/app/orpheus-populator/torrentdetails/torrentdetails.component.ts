import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {TorrentEntry, PathEntry} from '../datalist/torrent-data.service';
import {TorrentDetailsService} from './torrentdetails.service';
import {LayoutChildComponent} from '../layout/layout.component';


/**
 * Show detailed information about a torrent configured using the
 * TorrentDetails service.
 */
@Component({
  selector: 'app-torrentdetails',
  templateUrl: './torrentdetails.component.html',
  styleUrls: ['./torrentdetails.component.css']
})
export class TorrentdetailsComponent implements OnInit, LayoutChildComponent {
  @Input() position: string;
  entry: TorrentEntry;
  edition: [string, number, string, string];
  descriptionExpanded = false;
  constructor(private torrentDetailsService: TorrentDetailsService,
              private cdr: ChangeDetectorRef) {}
  ngOnInit(): void {
    const lastEntry = this.torrentDetailsService.getEntry(this.position);
    if (lastEntry) {
      this.setEntry(lastEntry);
    }
    this.torrentDetailsService.getObservable(this.position)
      .subscribe(x => this.setEntry(x));
  }
  private setEntry(entry: TorrentEntry): void {
    this.entry = entry;
    this.edition = entry.getEditionInfo();
    // FIXME: only needed when using GM.xmlHttpRequest
    // for reasons unknown
    this.cdr.detectChanges();
  }
  public rerender(): void {
    // FIXME: only needed when using GM.xmlHttpRequest
    // for reasons unknown
    this.cdr.detectChanges();
  }
}


/**
 * Display the file tree of a torrent.
 */
@Component({
  selector: 'app-torrentfiletree',
  templateUrl: './filetree.component.html',
  styleUrls: ['./filetree.component.css']
})
export class TorrentFileTreeComponent {
  @Input() path: PathEntry;
  childStatus = new Map<PathEntry, boolean>();
  constructor(private cdr: ChangeDetectorRef) {}
  getChildren(): PathEntry[] {
    return [...this.path.children.values()];
  }
  toggleChildTree(child: PathEntry): void {
    this.childStatus.set(child, !this.childStatus.get(child));
    // FIXME: only needed when using GM.xmlHttpRequest
    // for reasons unknown
    this.cdr.detectChanges();
  }
  showChild(child: PathEntry): boolean {
    return !!this.childStatus.get(child);
  }
}
