import {Injectable} from '@angular/core';
import {TorrentEntry} from '../datalist/torrent-data.service';
import {Observable, Subject} from 'rxjs';

/**
 * Saves state for the TorrentDetails component and provides cross-component
 * communication.
 */
@Injectable({
  providedIn: 'root'
})
export class TorrentDetailsService {
  private entryTracker = new Map<string, Subject<TorrentEntry>>();
  private lastTracker = new Map<string, TorrentEntry>();

  setEntry(entry: TorrentEntry, position: string): void {
    this.getSubject(position).next(entry);
    this.lastTracker.set(position, entry);
  }

  getEntry(position: string): TorrentEntry {
    return this.lastTracker.get(position);
  }

  getObservable(position: string): Observable<TorrentEntry> {
    return this.getSubject(position).asObservable();
  }

  private getSubject(position: string): Subject<TorrentEntry> {
    let subject = this.entryTracker.get(position);
    if (subject === undefined) {
      subject = new Subject<TorrentEntry>();
      this.entryTracker.set(position, subject);
    }
    return subject;
  }
}
